package fr.aityounes.zanas_crypto
import CoinGeckoApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Network {
    val coinGeckoApi: CoinGeckoApi by lazy {
        Retrofit.Builder()
            .baseUrl(CoinGeckoApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(CoinGeckoApi::class.java)
    }
}
