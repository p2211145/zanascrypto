package fr.aityounes.zanas_crypto


import CoinGeckoApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class CryptoRepository {
    private val coinGeckoApi = Retrofit.Builder()
        .baseUrl(CoinGeckoApi.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CoinGeckoApi::class.java)

    suspend fun getCryptos(): List<Crypto> {
        return coinGeckoApi.getCryptos()
    }

    suspend fun getCryptoDetail(cryptoId: String): CryptoDetail {
        val apiResponse = coinGeckoApi.getCryptoDetails(cryptoId)
        return apiResponse.toCryptoDetail()
    }

    suspend fun getMarketChart(cryptoId: String): MarketChartResponse {
        return coinGeckoApi.getMarketChart(cryptoId)
    }
}
