package fr.aityounes.zanas_crypto

data class CryptoDetailResponse(
    val id: String,
    val symbol: String,
    val name: String,
    val market_data: MarketData
) {
    data class MarketData(
        val current_price: CurrentPrice,
        val price_change_percentage_24h: Double,
        val price_change_percentage_7d: Double,
        val price_change_percentage_14d: Double,
        val price_change_percentage_30d: Double,
        val price_change_percentage_60d: Double,
        val price_change_percentage_200d: Double,
        val price_change_percentage_1y: Double
    )

    data class CurrentPrice(
        val usd: Double
    )

    fun toCryptoDetail(): CryptoDetail {
        return CryptoDetail(
            id = this.id,
            name = this.name,
            symbol = this.symbol,
            current_price = this.market_data.current_price.usd,
            image = "", // Assuming you will handle this properly
            price_change_percentage_24h = this.market_data.price_change_percentage_24h,
            market_data = CryptoDetail.MarketData(
                price_change_percentage_7d = this.market_data.price_change_percentage_7d,
                price_change_percentage_14d = this.market_data.price_change_percentage_14d,
                price_change_percentage_30d = this.market_data.price_change_percentage_30d,
                price_change_percentage_60d = this.market_data.price_change_percentage_60d,
                price_change_percentage_200d = this.market_data.price_change_percentage_200d,
                price_change_percentage_1y = this.market_data.price_change_percentage_1y
            )
        )
    }
}
