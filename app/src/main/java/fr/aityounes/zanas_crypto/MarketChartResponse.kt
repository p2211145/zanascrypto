package fr.aityounes.zanas_crypto

data class MarketChartResponse(
    val prices: List<List<Double>>
)
