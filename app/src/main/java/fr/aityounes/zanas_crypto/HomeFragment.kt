package fr.aityounes.zanas_crypto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerView)

        val adapter = CryptoAdapter(listOf()) { crypto ->
            val action = HomeFragmentDirections.actionNavigationHomeToNavigationDashboard(crypto.id)
            findNavController().navigate(action)
        }

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        homeViewModel.cryptosLiveData.observe(viewLifecycleOwner, Observer { cryptos ->
            adapter.cryptos = cryptos
            adapter.notifyDataSetChanged()
        })

        return view
    }
}
