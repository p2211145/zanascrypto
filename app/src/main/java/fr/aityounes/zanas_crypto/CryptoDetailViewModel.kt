package fr.aityounes.zanas_crypto

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class CryptoDetailViewModel : ViewModel() {
    private val cryptoRepository = CryptoRepository()

    private val _cryptoDetailLiveData = MutableLiveData<CryptoDetail>()
    val cryptoDetailLiveData: LiveData<CryptoDetail> = _cryptoDetailLiveData

    private val _marketChartLiveData = MutableLiveData<MarketChartResponse>()
    val marketChartLiveData: LiveData<MarketChartResponse> = _marketChartLiveData

    fun fetchCryptoDetails(cryptoId: String) {
        viewModelScope.launch {
            val cryptoDetail = cryptoRepository.getCryptoDetail(cryptoId)
            _cryptoDetailLiveData.value = cryptoDetail
        }
    }

    fun fetchMarketChart(cryptoId: String) {
        viewModelScope.launch {
            val marketChart = cryptoRepository.getMarketChart(cryptoId)
            _marketChartLiveData.value = marketChart
        }
    }
}
