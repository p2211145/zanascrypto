package fr.aityounes.zanas_crypto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer

class CryptoDetailFragment : Fragment() {

    private val viewModel: CryptoDetailViewModel by viewModels()

    private lateinit var cryptoName: TextView
    private lateinit var cryptoSymbol: TextView
    private lateinit var cryptoPrice: TextView

    companion object {
        private const val ARG_CRYPTO_ID = "cryptoId"

        fun newInstance(cryptoId: String): CryptoDetailFragment {
            val args = Bundle()
            args.putString(ARG_CRYPTO_ID, cryptoId)

            val fragment = CryptoDetailFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_crypto_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cryptoName = view.findViewById(R.id.crypto_name)
        cryptoSymbol = view.findViewById(R.id.crypto_symbol)
        cryptoPrice = view.findViewById(R.id.crypto_price)

        viewModel.cryptoDetailLiveData.observe(viewLifecycleOwner, Observer { cryptoDetail ->
            cryptoName.text = cryptoDetail.name
            cryptoSymbol.text = cryptoDetail.symbol
            cryptoPrice.text = cryptoDetail.current_price.toString()
        })

        val cryptoId = arguments?.getString(ARG_CRYPTO_ID)
        if (cryptoId != null) {
            viewModel.fetchCryptoDetails(cryptoId)
        }
    }
}
