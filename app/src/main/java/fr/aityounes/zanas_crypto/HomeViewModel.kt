package fr.aityounes.zanas_crypto

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers

class HomeViewModel : ViewModel() {
    private val cryptoRepository = CryptoRepository()
    val cryptosLiveData = liveData(Dispatchers.IO) {
        val cryptos = cryptoRepository.getCryptos()
        emit(cryptos)
    }
}
