import fr.aityounes.zanas_crypto.Crypto
import fr.aityounes.zanas_crypto.CryptoDetail
import retrofit2.http.GET
import retrofit2.http.Path

interface CoinGeckoApi {
    companion object {
        const val BASE_URL = "https://api.coingecko.com/api/v3/"
    }

    @GET("coins/markets?vs_currency=usd")
    suspend fun getCryptos(): List<Crypto>

    @GET("coins/{id}/market_chart?vs_currency=usd&days=30")
    suspend fun getMarketChart(@Path("id") id: String): fr.aityounes.zanas_crypto.MarketChartResponse

    @GET("coins/{id}")
    suspend fun getCryptoDetails(@Path("id") id: String): ApiCryptoDetailResponse
}


data class MarketChartResponse(
    val prices: List<List<Double>>,
    val market_caps: List<List<Double>>,
    val total_volumes: List<List<Double>>
)

data class ApiCryptoDetailResponse(
    val id: String,
    val symbol: String,
    val name: String,
    val market_data: MarketData
) {

    data class MarketData(
        val current_price: CurrentPrice,
        val price_change_percentage_24h: Double,
        val price_change_percentage_7d: Double,
        val price_change_percentage_14d: Double,
        val price_change_percentage_30d: Double,
        val price_change_percentage_60d: Double,
        val price_change_percentage_200d: Double,
        val price_change_percentage_1y: Double
    )

    data class CurrentPrice(
        val usd: Double
    )
}
