package fr.aityounes.zanas_crypto

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class CryptoAdapter(var cryptos: List<Crypto> = listOf(), private val onCryptoClick: (Crypto) -> Unit) : RecyclerView.Adapter<CryptoAdapter.CryptoViewHolder>() {

    class CryptoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cryptoNameTextView: TextView = itemView.findViewById(R.id.cryptoNameTextView)
        val cryptoPriceTextView: TextView = itemView.findViewById(R.id.cryptoPriceTextView)
        val cryptoLogoImageView: ImageView = itemView.findViewById(R.id.cryptoLogoImageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CryptoViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.crypto_item, parent, false)
        return CryptoViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CryptoViewHolder, position: Int) {
        val currentItem = cryptos[position]
        holder.cryptoNameTextView.text = currentItem.name
        holder.cryptoPriceTextView.text = currentItem.current_price.toString()

        Glide.with(holder.cryptoLogoImageView.context)
            .load(currentItem.image)
            .into(holder.cryptoLogoImageView)

        // Ajout d'un clic listener pour chaque élément de la liste.
        holder.itemView.setOnClickListener {
            onCryptoClick(currentItem)
        }
    }

    override fun getItemCount() = cryptos.size
}
