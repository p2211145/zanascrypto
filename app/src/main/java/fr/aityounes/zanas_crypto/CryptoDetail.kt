package fr.aityounes.zanas_crypto

data class CryptoDetail(
    val id: String,
    val symbol: String,
    val name: String,
    val current_price: Double,
    val image: String,
    val price_change_percentage_24h: Double,
    val market_data: MarketData
) {
    data class MarketData(
        val price_change_percentage_7d: Double,
        val price_change_percentage_14d: Double,
        val price_change_percentage_30d: Double,
        val price_change_percentage_60d: Double,
        val price_change_percentage_200d: Double,
        val price_change_percentage_1y: Double
    )
}
